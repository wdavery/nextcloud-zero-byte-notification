This script will search for 0 byte files in a Nextcloud instance and send an email containing a list of all matching files.  

It is intended to help minimize file loss due to a long outstanding Owncloud/Nextcloud bug. More info here: https://github.com/nextcloud/server/issues/3056#  
Recommended usage is to run as a daily cron job, and keep regular backups of your files.

Original credits to 'feisar' who posted this in 2017: https://help.nextcloud.com/t/files-become-zero-bytes/7214/17

----------

Example output:    
```
User/files/Photos/2020/09/0597.png
User/files/Photos/2020/09/0594.png
User/files/Photos/2020/09/0598.png
User/files/Photos/2020/09/0596.png
User/files/Photos/2020/09/1EA7.jpg
User/files/Photos/2020/08/B9FB.jpg
User/files/Photos/2020/08/0579.jpg
User/files/Photos/2020/08/0574.jpg
User/files/Photos/2020/08/0580.jpg
User/files/Photos/2020/08/0587.png
User/files/Photos/2020/05/FB3C.jpg
User/files/Photos/2020/10/0693.jpg
User/files/Photos/2020/10/0694.jpg
```